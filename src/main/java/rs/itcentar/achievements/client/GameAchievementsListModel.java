package rs.itcentar.achievements.client;

import java.util.List;
import javax.swing.DefaultListModel;

public class GameAchievementsListModel extends DefaultListModel<Achievement> {

    private List<Achievement> achievements;
    
    @Override
    public void clear() {
        achievements.clear();
    }

    @Override
    public void addElement(Achievement a) {
        achievements.add(a);
    }

    @Override
    public void removeElementAt(int i) {
        achievements.remove(i);
    }

    @Override
    public boolean isEmpty() {
        return achievements.isEmpty();
    }

    @Override
    public Achievement getElementAt(int i) {
        return achievements.get(i);
    }

    @Override
    public int getSize() {
        return achievements == null ? 0 : achievements.size();
    }
    
    public void update(List<Achievement> achievements) {
        this.achievements = achievements;
        fireContentsChanged(this, 0, this.achievements.size());
    }
    
    public void signalUpdate() {
        fireContentsChanged(this, 0, this.achievements.size());
    }
}
