package rs.itcentar.achievements.client;

import java.util.Date;

public class AchievementDetailsPanel extends javax.swing.JPanel {

    public AchievementDetailsPanel() {
        initComponents();
    }

    public void update(Achievement achievement) {
        if (achievement != null) {
            lId.setText(achievement.getId());
            lDisplayName.setText(achievement.getDisplayName());
            lDescription.setText(achievement.getDescription());
            lDisplayOrder.setText(String.valueOf(achievement.getDisplayOrder()));
            lCreated.setText(new Date(achievement.getCreated()).toString());
            lUpdated.setText(new Date(achievement.getUpdated()).toString());
            lIcon.setText(achievement.getIcon());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lId = new javax.swing.JLabel();
        lDisplayName = new javax.swing.JLabel();
        lDescription = new javax.swing.JLabel();
        lDisplayOrder = new javax.swing.JLabel();
        lCreated = new javax.swing.JLabel();
        lUpdated = new javax.swing.JLabel();
        lIcon = new javax.swing.JLabel();

        jLabel1.setText("ID:");

        lId.setText("id");

        lDisplayName.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lDisplayName.setText("DisplayName");

        lDescription.setText("Desription");
        lDescription.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        lDisplayOrder.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lDisplayOrder.setText("#");

        lCreated.setText("Created");

        lUpdated.setText("Updated");

        lIcon.setText("Icon");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lIcon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(lDescription, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lDisplayName, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lDisplayOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lCreated, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(40, 40, 40)
                        .addComponent(lUpdated, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lId))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lDisplayName)
                    .addComponent(lDisplayOrder))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lCreated)
                    .addComponent(lUpdated))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lIcon, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lCreated;
    private javax.swing.JLabel lDescription;
    private javax.swing.JLabel lDisplayName;
    private javax.swing.JLabel lDisplayOrder;
    private javax.swing.JLabel lIcon;
    private javax.swing.JLabel lId;
    private javax.swing.JLabel lUpdated;
    // End of variables declaration//GEN-END:variables

}
