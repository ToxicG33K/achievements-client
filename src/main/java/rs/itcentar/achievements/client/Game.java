package rs.itcentar.achievements.client;

import java.util.UUID;

public class Game {
    private String id;
    private String displayName;

    public Game(String displayName) {
        this.id = UUID.randomUUID().toString();
        this.displayName = displayName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }
}
