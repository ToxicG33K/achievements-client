package rs.itcentar.achievements.client;

import java.util.UUID;

public class Achievement {
    private String id;
    private String displayName;
    private String description;
    private int displayOrder;
    private long created;
    private long updated;
    private String icon;

    public Achievement(String displayName, String description, int displayOrder, long created, long updated, String icon) {
        this.id = UUID.randomUUID().toString();
        this.displayName = displayName;
        this.description = description;
        this.displayOrder = displayOrder;
        this.created = created;
        this.updated = updated;
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getUpdated() {
        return updated;
    }

    public void setUpdated(long updated) {
        this.updated = updated;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return displayName;
    }
}
