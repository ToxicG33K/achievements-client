package rs.itcentar.achievements.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataModel {

    private static List<Game> games;
    private static Map<String, List<Achievement>> gameAchievements = new HashMap<>();
    
    private static List<Achievement> cacheGameAchievements;

    public static void init() {
        games = new ArrayList<>(Arrays.asList(new Game[]{
            new Game("Game 1"),
            new Game("Game 2"),
            new Game("Game 3")
        }));
        
        for(Game g : games) {
            List<Achievement> achievements = new ArrayList<>();
            for(int i=0;i<5;i++) {
                achievements.add(new Achievement(g.getDisplayName() + " Achievement " + (i+1),
                        "Sample description", i+1, System.currentTimeMillis(), 0, "icon url"));
            }
            gameAchievements.put(g.getId(), achievements);
        }
    }
    
    public static List<Game> getGames() {
        return games;
    }
    
    public static List<Achievement> getGameAchievements(String gameId) {
        cacheGameAchievements = gameAchievements.get(gameId);
        return cacheGameAchievements;
    }
    
    public static void deleteAchievement(int index) {
        cacheGameAchievements.remove(index);
    }
    
    public static void addAchievement(Achievement achievement) {
        cacheGameAchievements.add(achievement);
    }
    
    public static void deleteGame(int index) {
        games.remove(index);
    }
    
    public static void addGame(Game game) {
        games.add(game);
        gameAchievements.put(game.getId(), new ArrayList<>());
    }
}
