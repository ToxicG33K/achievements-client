package rs.itcentar.achievements.client;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MainFrame extends javax.swing.JFrame {

    private GamesListModel gamesModel = new GamesListModel();
    private GameAchievementsListModel gameAchiemevemntsModel = new GameAchievementsListModel();
    private AchievementDetailsPanel detailsPanel = new AchievementDetailsPanel();
    private AchievementDetailsEditPanel detailsEditPanel = new AchievementDetailsEditPanel();

    public MainFrame() {
        DataModel.init();
        initComponents();

        detailsEditPanel.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent pce) {
                switch (pce.getPropertyName()) {
                    case AchievementDetailsEditPanel.PROPERTY_ACHIEVEMENT_CHANGED:
                        Achievement achievement = (Achievement) pce.getNewValue();
                        gameAchiemevemntsModel.signalUpdate();
                        showAchievementDetails(achievement);
                        break;
                    case AchievementDetailsEditPanel.PROPERTY_ACHIEVEMENT_CHANGE_CANCELED:
//                        Achievement a = gameAchiemevemntsModel.getElementAt(lGameAchievements.getSelectedIndex());
                        showAchievementDetails(null);
                        break;
                }
            }
        });

        gamesModel.update(DataModel.getGames());
        lGames.setModel(gamesModel);

        lGameAchievements.setModel(gameAchiemevemntsModel);

        lGames.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if (!lse.getValueIsAdjusting()) {
                    Game game = gamesModel.getElementAt(lGames.getSelectedIndex());
                    List<Achievement> gameAchievements = DataModel.getGameAchievements(game.getId());
                    gameAchiemevemntsModel.update(gameAchievements);
                    
                    // select first achievement
                    if(!gameAchiemevemntsModel.isEmpty()) {
                        lGameAchievements.clearSelection();
                        lGameAchievements.getSelectionModel().setSelectionInterval(0, 0);
                    }
                }
            }
        });

        lGameAchievements.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if (!lse.getValueIsAdjusting()) {
                    if (lGameAchievements.getSelectedIndex() > -1) {
                        Achievement achievement = gameAchiemevemntsModel.getElementAt(lGameAchievements.getSelectedIndex());
                        showAchievementDetails(achievement);
                    }
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        bCreateAchievement = new javax.swing.JButton();
        bDeleteAchievement = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lGameAchievements = new javax.swing.JList<>();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        bUpdateAchievement = new javax.swing.JButton();
        pDetailsContainer = new javax.swing.JPanel();
        spContainer = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lGames = new javax.swing.JList<>();
        bCreateGame = new javax.swing.JButton();
        bDeleteGame = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Achievements Client");
        setResizable(false);

        jSplitPane1.setDividerLocation(230);
        jSplitPane1.setResizeWeight(0.5);

        jSplitPane2.setDividerLocation(230);
        jSplitPane2.setResizeWeight(0.5);

        bCreateAchievement.setText("Create");
        bCreateAchievement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateAchievementActionPerformed(evt);
            }
        });

        bDeleteAchievement.setText("Delete");
        bDeleteAchievement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteAchievementActionPerformed(evt);
            }
        });

        jLabel2.setText("Game Achievements:");

        lGameAchievements.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(lGameAchievements);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addComponent(bCreateAchievement)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 81, Short.MAX_VALUE)
                        .addComponent(bDeleteAchievement)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bCreateAchievement)
                    .addComponent(bDeleteAchievement))
                .addContainerGap())
        );

        jSplitPane2.setLeftComponent(jPanel2);

        jLabel3.setText("Achievements Details:");

        bUpdateAchievement.setText("Update");
        bUpdateAchievement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bUpdateAchievementActionPerformed(evt);
            }
        });

        pDetailsContainer.setLayout(new java.awt.BorderLayout());
        pDetailsContainer.add(spContainer, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pDetailsContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(bUpdateAchievement))
                        .addGap(0, 402, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pDetailsContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bUpdateAchievement)
                .addContainerGap())
        );

        jSplitPane2.setRightComponent(jPanel3);

        jSplitPane1.setRightComponent(jSplitPane2);

        jLabel1.setText("Games:");

        lGames.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(lGames);

        bCreateGame.setText("Create");
        bCreateGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateGameActionPerformed(evt);
            }
        });

        bDeleteGame.setText("Delete");
        bDeleteGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteGameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(bCreateGame)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 81, Short.MAX_VALUE)
                        .addComponent(bDeleteGame)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bCreateGame)
                    .addComponent(bDeleteGame))
                .addContainerGap())
        );

        jSplitPane1.setLeftComponent(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void bUpdateAchievementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bUpdateAchievementActionPerformed
        Achievement achievement = gameAchiemevemntsModel
                .getElementAt(lGameAchievements.getSelectedIndex());
        showAchievementDetailsEdit(achievement);
    }//GEN-LAST:event_bUpdateAchievementActionPerformed

    private void bDeleteAchievementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteAchievementActionPerformed
        if (lGameAchievements.getSelectedIndex() > -1) {
            int ret = JOptionPane.showConfirmDialog(this, "Are you sure ?!", "Achievement Delete", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (ret == JOptionPane.YES_OPTION) {
                DataModel.deleteAchievement(lGameAchievements.getSelectedIndex());
                gameAchiemevemntsModel.signalUpdate();
                lGameAchievements.clearSelection();
            }
        }
    }//GEN-LAST:event_bDeleteAchievementActionPerformed

    private void bCreateAchievementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateAchievementActionPerformed
        if (lGames.getSelectedIndex() > -1) {
            AchievementCreatePanel panel = new AchievementCreatePanel();
            int ret = JOptionPane.showConfirmDialog(this, panel, "Achievement Creation", JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.PLAIN_MESSAGE);
            if (ret == JOptionPane.OK_OPTION) {
                Achievement achievement = panel.getAchievement();
                DataModel.addAchievement(achievement);
                gameAchiemevemntsModel.signalUpdate();
                lGameAchievements.getSelectionModel().setSelectionInterval(gameAchiemevemntsModel.getSize() - 1,
                        gameAchiemevemntsModel.getSize() - 1);
            }
        }
    }//GEN-LAST:event_bCreateAchievementActionPerformed

    private void bDeleteGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteGameActionPerformed
        if (lGames.getSelectedIndex() > -1) {
            int ret = JOptionPane.showConfirmDialog(this, "Are you sure ?!", "Game Delete", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (ret == JOptionPane.YES_OPTION) {
                DataModel.deleteGame(lGames.getSelectedIndex());
                gamesModel.signalUpdate();
                lGames.getSelectionModel().setSelectionInterval(gamesModel.getSize() - 1,
                        gamesModel.getSize() - 1);
            }
        }
    }//GEN-LAST:event_bDeleteGameActionPerformed

    private void bCreateGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateGameActionPerformed
        String displayName = JOptionPane.showInputDialog(this, "Enter Game Name",
                "Create Game", JOptionPane.PLAIN_MESSAGE);
        if (displayName == null || displayName.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Name can not be empty!",
                    "Create Game Error", JOptionPane.ERROR_MESSAGE);
        } else {
            Game game = new Game(displayName);
            DataModel.addGame(game);
            gamesModel.signalUpdate();
            lGames.getSelectionModel().setSelectionInterval(gamesModel.getSize() - 1, gamesModel.getSize() - 1);
        }
    }//GEN-LAST:event_bCreateGameActionPerformed

    public void showAchievementDetails(Achievement achievement) {
        detailsPanel.update(achievement);
        spContainer.getViewport().removeAll();
        spContainer.getViewport().add(detailsPanel);
        spContainer.revalidate();
        spContainer.repaint();
        pDetailsContainer.revalidate();
        pDetailsContainer.repaint();
    }

    public void showAchievementDetailsEdit(Achievement achievement) {
        detailsEditPanel.update(achievement);
        spContainer.getViewport().removeAll();
        spContainer.getViewport().add(detailsEditPanel);
        spContainer.revalidate();
        spContainer.repaint();
        pDetailsContainer.revalidate();
        pDetailsContainer.repaint();
    }

    public static void main(String args[]) {
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bCreateAchievement;
    private javax.swing.JButton bCreateGame;
    private javax.swing.JButton bDeleteAchievement;
    private javax.swing.JButton bDeleteGame;
    private javax.swing.JButton bUpdateAchievement;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JList<Achievement> lGameAchievements;
    private javax.swing.JList<Game> lGames;
    private javax.swing.JPanel pDetailsContainer;
    private javax.swing.JScrollPane spContainer;
    // End of variables declaration//GEN-END:variables
}
