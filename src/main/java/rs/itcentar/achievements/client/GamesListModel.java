package rs.itcentar.achievements.client;

import java.util.List;
import javax.swing.DefaultListModel;

public class GamesListModel extends DefaultListModel<Game> {

    private List<Game> games;
    
    @Override
    public void clear() {
        games.clear();
    }

    @Override
    public void addElement(Game g) {
        games.add(g);
    }

    @Override
    public void removeElementAt(int i) {
        games.remove(i);
    }

    @Override
    public boolean isEmpty() {
        return games.isEmpty();
    }

    @Override
    public Game getElementAt(int i) {
        return games.get(i);
    }

    @Override
    public int getSize() {
        return games == null ? 0 : games.size();
    }
    
    public void update(List<Game> games) {
        this.games = games;
        fireContentsChanged(this, 0, this.games.size());
    }
    
    public void signalUpdate() {
        fireContentsChanged(this, 0, this.games.size());
    }
}
